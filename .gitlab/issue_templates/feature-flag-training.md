## Feature Flag Training

[Feature Flags](https://docs.gitlab.com/ee/development/feature_flags/) are used by GitLab engineers in order to roll out changes while 
monitoring the impact. They are **not the same** as the [**Operations > Feature Flag**](https://docs.gitlab.com/ee/user/project/operations/feature_flags.html) feature inside of GitLab. This template is intended to help understand 
how and when to use this tool in development.

### Steps

- [ ] Assign this issue to yourself with the title of Feature Flag Training - First Name Last Name - Q#YYYY
- [ ] [Get access](https://docs.gitlab.com/ee/development/feature_flags/controls.html#access) to control feature flags

### Review

- [ ] [Standards](https://docs.gitlab.com/ee/development/feature_flags/process.html#feature-flags-in-gitlab-development)
- [ ] [When to use feature flags](https://docs.gitlab.com/ee/development/feature_flags/process.html#when-to-use-feature-flags)
- [ ] [Developing with feature flags](https://docs.gitlab.com/ee/development/feature_flags/development.html)
- [ ] [Enabling a feature flag locally](https://docs.gitlab.com/ee/development/feature_flags/development.html#enabling-a-feature-flag-in-development)
- [ ] [Including a feature behind a feature flag in the final release](https://docs.gitlab.com/ee/development/feature_flags/process.html#including-a-feature-behind-feature-flag-in-the-final-release)
- [ ] [Controlling feature flags](https://docs.gitlab.com/ee/development/feature_flags/controls.html)

### Watch (Coming Soon)
- [ ] Starting with Feature Flags
- [ ] Process around adding/removing a feature flag
- [ ] Working with existing feature flags
- [ ] The importance of maintaining feature flags
