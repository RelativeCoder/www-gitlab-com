---
layout: markdown_page
title: "Strategic Marketing On-Boarding and Other How-to's"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Strategic Marketing How-to's
* [Group Conversation deck](/handbook/marketing/product-marketing/getting-started/group-conversations/)

## Strategic Marketing General On-Boarding
* [Getting Started 101 - No Tissues with Issues](/handbook/marketing/product-marketing/getting-started/101/)
* [Getting Started 102 - Working at local speed](/handbook/marketing/product-marketing/getting-started/102/)
* [Getting Started 103 - Maintaining common slides across decks](/handbook/marketing/product-marketing/getting-started/103/)
* [Getting Started 104 - Epic Templates - Repeatable sets of issues](/handbook/marketing/product-marketing/getting-started/104/)
* [Getting Started 105 - Label Hygiene - Keeping your issues squeaky clean](/handbook/marketing/product-marketing/getting-started/105/)
* [Keeping Yourself Informed](/handbook/marketing/product-marketing/getting-started/communication/)
* [Searching the GitLab Website Like a Pro](/handbook/tools-and-tips/searching/)

* [Getting Started xxx - Project management Overview](/handbook/marketing/product-marketing/getting-started/sm-project-management/)

## Teams Specific

### Competitive intelligence

### Market Research & Customer insight

### Partner & Channel Marketing

### Product marketing

* [Go to the PMM onboarding page](/handbook/marketing/product-marketing/pmmteam/onboarding) and check out our onboarding guide.

### Technical marketing
* [Creating a Kubernetes Cluster](/handbook/marketing/product-marketing/getting-started/create-cluster-101/)
