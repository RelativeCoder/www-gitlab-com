---
layout: markdown_page
title: "CI demos"
noindex: true
---

### See how GitLab meets the CI use case [market requirements](/handbook/marketing/product-marketing/usecase-gtm/ci/#market-requirements)


#### 1) Build and test automation

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/rti7T1yGrlw" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

#### 2) Configuration management

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/opdLqwz6tcE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

#### 3) Easy to get started

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/e0iQD1qgxZg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

#### 4) Platform and language support

Demo to be added

#### 5) Visibility and collaboration

Demo to be added

#### 6) DevOps tools integrations

Demo to be added

#### 7) Pipeline security

Demo to be added

#### 8) Analytics

Demo to be added

#### 9) Elastic scalability

Demo to be added
