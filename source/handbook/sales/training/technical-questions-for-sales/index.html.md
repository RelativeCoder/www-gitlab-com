---
layout: handbook-page-toc
title: "Technical Questions for Sales"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## **Introduction**
To reduce the dependence of GitLab sales team members on Solution Architects (SAs), the SA team created the below table outlining some of the most common technical questions that they believe GitLab sales team members should be able to answer.

### **GitLab Features**

| **Question** | **Answer** |
| ------ | ------ |
| What authentication options are available when using .com? | Check out the [System for Cross-domain Identity Management (SCIM) documentation](https://docs.gitlab.com/ee/user/group/saml_sso/scim_setup.html) |
| What are the best ways to achieve high availability? | View the [GitLab Jira Integration page](/solutions/jira/) | 
| What are the best ways to achieve disaster recovery? | Review the [Replication with Geo documentation](https://docs.gitlab.com/ee/administration/geo/replication/index.html) | 
| What are the GitLab security capabilities? | GitLab can check your application for security vulnerabilities that may lead to unauthorized access, data leaks, denial of services, and more. [Learn more](https://docs.gitlab.com/ee/user/application_security/). | 
| How does GitLab CI work? | Continuous Integration works by pushing small code chunks to your application’s code base hosted in a Git repository, and, to every push, run a pipeline of scripts to build, test, and validate the code changes before merging them into the main branch. [Learn more](https://docs.gitlab.com/ee/ci/). | 
| How can we do test management with GitLab? | The GitLab Product Team is exploring enhancements in this area. Check out the [Quality Management Direction page](/direction/plan/quality_management/). | 
| What options do we have to manage user access on .com? | Check out the [SAML SSO for GitLab.com groups documentation](https://docs.gitlab.com/ee/user/group/saml_sso/) | 
| When will we no longer rely on using Docker-in-Docker (DinD) for security scanners? | This is an ongoing effort. See the [Use non-DinD mode by default for Security Products issue](https://gitlab.com/gitlab-org/gitlab/-/issues/37278) for more information. |
| What best practices exist around using projects/groups? | Watch [this video](https://www.youtube.com/watch?v=VR2r1TJCDew) (Jan 2019, 15 minutes) |
| What are runners and how do they actually work? | GitLab Runner is the open source project that is used to run your jobs and send the results back to GitLab. It is used in conjunction with [GitLab CI/CD](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/), the open-source continuous integration service included with GitLab that coordinates the jobs. Learn more by reading the [GitLab Runner documentation](https://docs.gitlab.com/runner/). |
| For what purpose or use case may the package repositories in GitLab be used? Will they replace Nexus or Artifactory? | Check out the [JFrog Artifactory vs. GitLab comparison](/devops-tools/jfrog-artifactory-vs-gitlab.html) |

### **Integrations**

| **Question** | **Answer** |
| ------ | ------ |
| How can we integrate Jira? | Check out the [GitLab Jira Integration page](/solutions/jira/) |
| What is GitLab's out-of-the-box support for (AWS/Azure/GCP)? | Please look at GitLab's [multi cloud support](/multicloud/) | 
  
### **Implementation**

| **Question** | **Answer** |
| ------ | ------ |
| Does GitLab have reference architectures with sizing information? | GitLab supports a number of scaling options to ensure that your self-managed instance is able to scale out to meet your organization’s needs when scaling up a single-box GitLab installation is no longer practical or feasible. Check out [GitLab's Reference Architecture documentation](https://docs.gitlab.com/ee/administration/scaling/index.html#reference-architectures). |
| How should customers set up and customize user roles in GitLab? | There are multiple types of permissions across GitLab, and when implementing anything that deals with permissions, all of them should be considered. Learn more with the [GitLab permissions guide](https://docs.gitlab.com/ee/development/permissions.html). | 
| What are the best practices for backing up a GitLab instance? | Check out the [Backing up and restoring GitLab documentation](https://docs.gitlab.com/ee/raketasks/backup_restore.html) |
| How may customers move from SVN to git? | Read the [Migrating from SVN to GitLab documentation](https://docs.gitlab.com/ee/user/project/import/svn.html) |
| How can users be auto-provisioned? | Review the [Create users through integrations documentation](https://docs.gitlab.com/ee/user/profile/account/create_accounts.html#create-users-through-integrations) |
| What are best practices for a runner infrastructure? | Learn more [here](https://docs.gitlab.com/runner/best_practice/) |
| What installation method does GitLab recommend customers use? | GitLab strongly recommend downloading the Omnibus package installation since it is quicker to install, easier to upgrade, and it contains features to enhance reliability not found in other methods. We also strongly recommend [at least 4GB of free RAM](https://docs.gitlab.com/ee/install/requirements.html#cpu) to run GitLab. Learn more on the [GitLab Installation page](/install/).  |
| What are best practices for a developer workflow? | Take a look at [Introduction to GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html) |
  
### **Product Tiering**

| **Question** | **Answer** |
| ------ | ------ |
| How do customers migrate from GitLab CE to EE? | Check out the [Upgrade to Enterprise Edition page](/upgrade/) |
| How do customers migrate from GitLab self-managed to .com? | Find out how on the [Upgrade to Enterprise Edition page](https://docs.gitlab.com/ee/user/project/import/#migrating-from-self-managed-gitlab-to-gitlabcom) |
| What is the difference between the Jira integration in Starter and in Premium? | Review the [GitLab Jira development panel integration](https://docs.gitlab.com/ee/integration/jira_development_panel.html) documentation |
| What are the differences around merge request approvals between the EE tiers? | Learn more on the [GitLab Create Features page](https://about.gitlab.com/features/#create) |
| How is user management different on .com compared to self-managed? | Administrators have more powerful user management capabilities. Check out [all the differences between GitLab.com and self-managed](/handbook/marketing/product-marketing/dot-com-vs-self-managed/#all-differences-between-gitlabcom-and-self-managed). |